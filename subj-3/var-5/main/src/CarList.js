import React from 'react';
import AddCar from './AddCar.js';
export class CarList extends React.Component {
    constructor(){
        super();
        this.state = {
            cars: []
        };
        this.addCar = (car) => {
			this.setState( prevState => ({
                cars: [...prevState.cars, car]
            }));
		}
    }   

    render(){
        return (
            <div>
             <AddCar onAdd={this.addCar}/>
            </div>
        )
    }
}