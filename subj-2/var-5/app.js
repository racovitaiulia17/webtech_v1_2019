const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.locals.products = [
    {
        name: "Iphone XS",
        category: "Smartphone",
        price: 5000
    },
    {
        name: "Samsung Galaxy S10",
        category: "Smartphone",
        price: 3000
    },
    {
        name: "Huawei Mate 20 Pro",
        category: "Smartphone",
        price: 3500
    }
];

app.get('/products', (req, res) => {
    res.status(200).json(app.locals.products);
});

app.post('/products', (req, res, next) => {
   // res.status(500).json({message: 'Bad request'});
    try {
        // TODO
        const product = req.body;
        if (Object.keys(product).length === 0) {
            res.status(500).send({ message: 'Body is missing' })
        }
        else if (product.name == null || product.category == null || product.price == null) {
            res.status(500).send({ message: 'Invalid body format' })
        }
        else if (product.price < 0) {
            res.status(500).send({ message: 'Price should be a positive number' })
        }
        else if (product.name) {
            res.status(500).send({ message: 'Product already exists' })
        }
        else {
            //let resp = await Product.create(product);
            //res.status(201).send({ message: 'created' })
        }
            
    }
    catch (err) {
        console.warn(err.stack)
        res.status(500).json({ message: 'server error' })
    }
})

module.exports = app;